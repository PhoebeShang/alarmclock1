package com.example.hw_alarmclock1;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Service;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AlertAlarmActivity extends AppCompatActivity {

    int count = 0;
    MediaPlayer mMediaPlayer;
    MyService myService;
    AlarmDB alarmDB;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_alarm);

        alarmDB = new AlarmDB(this);

        TextView mShowAlarmTime = (TextView) findViewById(R.id.showAlarmTime);
        Button closeButton = (Button) findViewById(R.id.closeButton);
        Button laterButton = (Button) findViewById(R.id.laterButton);
        closeButton.setBackgroundColor(getColor(R.color.darkred));
        laterButton.setBackgroundColor(getColor(R.color.darkred));

        Intent intentFromReceiver = getIntent();
        String alarmTime = intentFromReceiver.getStringExtra("TIMEINFO");
        int btnId = intentFromReceiver.getIntExtra("btnID", -1);
        mShowAlarmTime.setText(alarmTime);

        startAlarm();

        ServiceConnection serviceConn = new ServiceConnection() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                myService = ((MyService.MyBinder) iBinder).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
            }
        };
        Intent serviceIntent = new Intent(this, MyService.class);
//        serviceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        bindService(serviceIntent, serviceConn, Service.BIND_AUTO_CREATE);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //關閉app
                updateCheck(btnId, false);
                finishAndRemoveTask();
                stopAlarm();

            }
        });

        laterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //過10分鐘再響一次
                finishAndRemoveTask();
                stopAlarm();

                int setHour = 0;
                Date newSetDate = null;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm a");

                String[] setTime = alarmTime.split("\\s|:");
                Calendar day = Calendar.getInstance();

                try {
                    setHour = Integer.parseInt(setTime[0]);
                    String newSetTime = day.get(Calendar.YEAR) + "/" + (day.get(Calendar.MONTH) + 1)
                            + "/" + day.get(Calendar.DATE)
                            + " " + setHour + ":"+ (Integer.parseInt(setTime[1])+10) + " " + setTime[2];
                    newSetDate = sdf.parse(newSetTime);

                } catch (ParseException ex) {
                    ex.getMessage();
                }
                sdf = new SimpleDateFormat("hh:mm a");

                System.out.println("time: " + newSetDate);
                myService.setClock(btnId, 10*60*1000L, sdf.format(newSetDate));
            }
        });

    }

    private void startAlarm() {
        mMediaPlayer = MediaPlayer.create(this, getSystemDefultRingtoneUri());
        mMediaPlayer.setLooping(false);
        if (count == 0) {
            mMediaPlayer.start();
            count++;
        }
        else
            mMediaPlayer.pause();
    }

    private void stopAlarm(){
        if(mMediaPlayer!= null)
            mMediaPlayer.stop();
    }

    private Uri getSystemDefultRingtoneUri() {
        return RingtoneManager.getActualDefaultRingtoneUri(this,RingtoneManager.TYPE_RINGTONE);
    }

    private void updateCheck(int viewId,boolean checkOrNot){
        SQLiteDatabase db = alarmDB.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("CHECK_OR_NOT", checkOrNot);
        db.update("MyAlarm",values,"VIEW_ID="+viewId,null);
    }
}