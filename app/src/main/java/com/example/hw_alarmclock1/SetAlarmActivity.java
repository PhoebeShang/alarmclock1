package com.example.hw_alarmclock1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

public class SetAlarmActivity extends AppCompatActivity {

    public static final String ALL_TIME = "";
    StringBuilder allTime = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alarm);

        EditText setHour = (EditText) findViewById(R.id.leftText);
        EditText setMin = (EditText) findViewById(R.id.rightText);
        RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup);
        Button btn = (Button) findViewById(R.id.button);
        Editable hour = setHour.getText();
        Editable min = setMin.getText();

        //get從ShowAlarmActivity傳來的intent
        Intent intent = getIntent();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (rg.getCheckedRadioButtonId()){
                    case R.id.amButton:
                        allTime.append(pad(hour)).append(":").append(pad(min))
                                .append(" ").append("AM");
                        break;
                    case R.id.pmButton:
                        allTime.append(pad(hour)).append(":").append(pad(min))
                                .append(" ").append("PM");
                        break;
                }
                intent.putExtra(ALL_TIME, allTime.toString());
                //resultCode 自定義 例如: 0代表成功跳轉 1代表跳轉失敗
                setResult(0,intent);
                finish();
            }
        });
    }

    //EditText中小於10的數字補0
    private static String pad(Editable c){
        if(c.length() == 2)
            return c.toString();
        else
            return "0"+ c.toString() ;
    }

    //EditText輸入範圍: 1-12
//        不知道如何比較Editable類型
//        if(Integer.parseInt(hour.toString()) > 12){
//        Toast.makeText(this,"The number range is from 0 to 12",
//                Toast.LENGTH_SHORT).show();
//    }

}