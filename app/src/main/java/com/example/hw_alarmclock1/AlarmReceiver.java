package com.example.hw_alarmclock1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        intent.setClass(context,AlertAlarmActivity.class);
        //對BroadcastReceiver來說，呼叫一個activity，即是產生一個新的task(FLAG_ACTIVITY_NEW_TASK)
        //FLAG_ACTIVITY_CLEAR_TASK 清除前面activity stack
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}

//    //動態註冊broadcastReceiver
//    IntentFilter itFilter = new IntentFilter("timeInfo");
//    AlarmReceiver broadcastReceiver = new AlarmReceiver();
//    registerReceiver(broadcastReceiver,itFilter);