package com.example.hw_alarmclock1;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MyService extends Service {
    public MyService() {
    }

    //一個是紀錄btnId對應哪個thread
    Map<Integer, Thread> alarmThreadMap = new HashMap<>();
    //一個是用來check此btnId是否存在
    Map<Integer, Boolean> alarmCheck = new HashMap<>();
    MyBinder myBinder = new MyBinder();

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return myBinder;
    }

    public class MyBinder extends Binder {
        public MyService getService() {
            return MyService.this;
        }
    }

    public void setClock(int id,long time, String timeInfo){
        Thread setThread = new Thread(){
            @Override
            public void run(){
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent intentToReceiver = new Intent("com.example.hw_alarmclock1.SENDTOREC")
                        .setPackage("com.example.hw_alarmclock1");
                intentToReceiver.putExtra("TIMEINFO",timeInfo);
                intentToReceiver.putExtra("btnID",id);
//                intentToReceiver.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if(alarmCheck.get(id))
                    sendBroadcast(intentToReceiver);
            }
        };
        alarmThreadMap.put(id, setThread);
        alarmCheck.put(id, true);
        setThread.start();
    }

    public void closeClock(int id){
        if(alarmThreadMap.containsKey(id)){
            Thread thread = alarmThreadMap.get(id);
            alarmCheck.put(id, false);
            thread.interrupt();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}