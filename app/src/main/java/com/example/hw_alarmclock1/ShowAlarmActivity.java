package com.example.hw_alarmclock1;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
//import android.app.NotificationChannel;
//import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ShowAlarmActivity extends AppCompatActivity {

    TextView mTime1,mTime2,mTime3;
    CheckBox mBox1,mBox2,mBox3;
    static ServiceConnection serviceConn;
    static MyService myService;
    Map<CheckBox, TextView> textMap = new HashMap<>();
    private AlarmDB alarmDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_alarm);

        //開啟SQLiteDB
        openDB();

        mTime1 = (TextView) findViewById(R.id.time1);
        mTime2 = (TextView) findViewById(R.id.time2);
        mTime3 = (TextView) findViewById(R.id.time3);
        mBox1 = (CheckBox) findViewById(R.id.box1);
        mBox2 = (CheckBox) findViewById(R.id.box2);
        mBox3 = (CheckBox) findViewById(R.id.box3);

        mTime1.setOnClickListener(this::onClick);
        mTime2.setOnClickListener(this::onClick);
        mTime3.setOnClickListener(this::onClick);

        mBox1.setOnCheckedChangeListener(this::onCheckedChanged);
        mBox2.setOnCheckedChangeListener(this::onCheckedChanged);
        mBox3.setOnCheckedChangeListener(this::onCheckedChanged);

        textMap.put(mBox1, mTime1);
        textMap.put(mBox2, mTime2);
        textMap.put(mBox3, mTime3);

//        Intent intentToService = new Intent();
//        intentToService.setClass(this, MyService.class);
//        startService(intentToService);

        serviceConn = new ServiceConnection() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            //當與Service連線建立後才觸發onServiceConnected()
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                myService = ((MyService.MyBinder) iBinder).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
            }
        };
        Intent intentToService = new Intent(this, MyService.class);
        bindService(intentToService, serviceConn, Service.BIND_AUTO_CREATE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        loadFromDB();
    }

    public void onClick(View v) {
        Intent intentToSet = new Intent();
        intentToSet.setClass(ShowAlarmActivity.this, SetAlarmActivity.class);
        intentToSet.putExtra("viewId", v.getId());
        //requestCode 自定義 需大於0 分辨跳至哪個activity
        startActivityForResult(intentToSet, 1);
    }

    //startActivityForResult() -> finish() -> onActivityResult()
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        int viewId = data.getIntExtra("viewId", -1);
        String timeMsg = data.getStringExtra(SetAlarmActivity.ALL_TIME);
        switch (viewId) {
            case R.id.time1:
                mTime1.setText(timeMsg);
                break;
            case R.id.time2:
                mTime2.setText(timeMsg);
                break;
            case R.id.time3:
                mTime3.setText(timeMsg);
                break;
            default:
                break;
        }

        addToDB(viewId,timeMsg);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onCheckedChanged(CompoundButton c, boolean isChecked) {

        updateCheck(textMap.get(c).getId(),isChecked);

        if (isChecked) {
            int setHour = 0;
            Date newSetDate = null;
            //一定要設定年月日，否則預設會是1970/01/01
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm a");
//            String[] setTime = textMap.get(c).getText().toString().split("\\s|:");
            String setTime = textMap.get(c).getText().toString();
            Calendar nowTime = Calendar.getInstance();

            try {
                String newSetTime = nowTime.get(Calendar.YEAR) + "/"
                        + (nowTime.get(Calendar.MONTH) + 1)
                        + "/" + nowTime.get(Calendar.DATE)
                        + " " + setTime;
                newSetDate = sdf.parse(newSetTime);
            }catch (ParseException ex){
                ex.printStackTrace();
            }

//            try {
//                if (setTime[2].equals("PM")) {
//                    setHour += (Integer.parseInt(setTime[0]) + 12);
//                    String newSetTime = nowTime.get(Calendar.YEAR) + "/"
//                            + (nowTime.get(Calendar.MONTH) + 1)
//                            + "/" + nowTime.get(Calendar.DATE)
//                            + " " + setHour + ":" + setTime[1]
//                            + " " + setTime[2];
//                    newSetDate = sdf.parse(newSetTime);
//                    System.out.println("sethour"+setHour);
//                } else {
//                    setHour = Integer.parseInt(setTime[0]);
//                    String newSetTime = nowTime.get(Calendar.YEAR) + "/"
//                            + (nowTime.get(Calendar.MONTH) + 1)
//                            + "/" + nowTime.get(Calendar.DATE)
//                            + " " + setHour + ":" + setTime[1]
//                            + " " + setTime[2];
//                    newSetDate = sdf.parse(newSetTime);
//                }
//            } catch (ParseException ex) {
//                ex.printStackTrace();
//            }

            long time = newSetDate.getTime() - (new Date().getTime());
            if (time < 0)
                time += 86400 * 1000;
            if(myService != null)
                myService.setClock(textMap.get(c).getId(),time, textMap.get(c).getText().toString());
        } else {
            myService.closeClock(c.getId());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        closeDB();
    }

    private void openDB(){
        alarmDB = new AlarmDB(this);
    }

    private void closeDB(){
        alarmDB.close();
    }

    private void loadFromDB(){
        SQLiteDatabase db = alarmDB.getWritableDatabase();
        //Cursor資料指標
        Cursor cursor = db.rawQuery("SELECT * FROM MyAlarm",null);
        while(cursor.moveToNext()) {
            //1: VIEW_ID
            //2: TIME
            //3: CHECK_OR_NOT
            int viewId = cursor.getInt(1);
            switch(viewId){
                case R.id.time1:
                    mTime1.setText(cursor.getString(2));
                    //cursor中沒有直接get boolean方法
                    mBox1.setChecked(cursor.getInt(3) > 0);
                    break;
                case R.id.time2:
                    mTime2.setText(cursor.getString(2));
                    mBox2.setChecked(cursor.getInt(3) > 0);
                    break;
                case R.id.time3:
                    mTime3.setText(cursor.getString(2));
                    mBox3.setChecked(cursor.getInt(3) > 0);
                    break;
                default:
                    break;
            }
        }
    }

    private void addToDB(int viewId, String timeMsg){
        SQLiteDatabase db = alarmDB.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("VIEW_ID",viewId);
        values.put("TIME",timeMsg);
        db.replace("MyAlarm",null,values);
    }

    private void updateCheck(int viewId,boolean checkOrNot){
        SQLiteDatabase db = alarmDB.getWritableDatabase();
        //ContentValues 用來裝新增的資料
        ContentValues values = new ContentValues();
        values.put("CHECK_OR_NOT", checkOrNot);
//        int count =
                db.update("MyAlarm",values,"VIEW_ID="+viewId,null);
//        System.out.println("viewId is "+ viewId +", update count is " + count);
    }


}
